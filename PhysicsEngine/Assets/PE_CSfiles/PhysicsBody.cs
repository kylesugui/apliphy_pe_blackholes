﻿using UnityEngine;
using System.Collections;

public class PhysicsBody : MonoBehaviour
{
    RigidBody rb;

    public float gravity;
    // Initialization
    void Start()
    {
        rb = GetComponent<RigidBody>();
        
    }

    void FixedUpdate()
    {

        //Applied Gravity and another force here.
        ApplyGravity();
        //Update positions and velocity.

        //Call collision detection

        //Apply Collisions.
    }

    void ApplyGravity()
    {
        transform.position += new Vector3(0.0f, -gravity);
    }

    void ApplyFriction()
    {

    }

}
