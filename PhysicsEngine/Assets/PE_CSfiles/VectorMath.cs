﻿using UnityEngine;
using System.Collections;

public class VectorMath : MonoBehaviour {

    public void AddForce(GameObject obj, Vector2 dir, float force)
    {
        obj.transform.position = (dir * force); // Equivalent of object.AddForce(direction * amount of force)
    }

    //Returns a normalized vector.
    public static Vector2 normalize(float x, float y)
    {
        Vector2 normalized = new Vector2();
        float Sqrt = Mathf.Sqrt((x * x) + (y * y)); //Pythagorean Theorem;

        return normalized = new Vector2(x / Sqrt, y / Sqrt); 
        
    }

    public static float sqr(float x)
    {
        float sqrd = x * x;
        return sqrd;
    }
}
