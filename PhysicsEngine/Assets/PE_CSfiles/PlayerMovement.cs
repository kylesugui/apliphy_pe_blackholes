﻿using UnityEngine;
using System.Collections;

//For Dynamic RB
public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rb;
    public GameObject player;
    
    public float maxSpeed;
    public float speed;
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CharacterControl();
    }

    void CharacterControl()
    {
        if (Input.GetKey(KeyCode.A))
        {
            if (speed < maxSpeed)
                rb.AddForce(-transform.right * speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.D))
        {
            if (speed < maxSpeed)
                rb.AddForce(transform.right * speed * Time.deltaTime);
        }
    }
}
