﻿using UnityEngine;
using System.Collections;

public struct Circle
{
    public float Radius;
    public Vector2 Position; //Center
}

public struct Square
{
    public Vector2 min;
    public Vector2 max;
}

public class CollisionDetection : MonoBehaviour
{

    public VectorMath math;

    //If two circles collide, distance between the center of two circles is equal to r1 + r1, if distance is equal to r1 + r2, collision is detected.

    public bool CircleCollision(Circle a, Circle b)
    {
        float r = (a.Radius + b.Radius) * (a.Radius + b.Radius);
        return r >= ((b.Position.x - a.Position.x) * (b.Position.x - a.Position.x)) + ((a.Position.y - b.Position.y) * (a.Position.y - b.Position.y));
    }

    //Checks all possible overlapping edges, if one edge overlaps collision is detected.
    public bool AABB(Square a, Square b)
    {
        float d1x = b.min.x - a.max.x;
        float d1y = b.min.y - a.max.y;
        float d2x = a.min.x - b.max.x;
        float d2y = a.min.y - b.max.y;

        if (d1x > 0.0f || d1y > 0.0f)
            return false;

        if (d2x > 0.0f || d2y > 0.0f)
            return false;

        return true;
    }
}
