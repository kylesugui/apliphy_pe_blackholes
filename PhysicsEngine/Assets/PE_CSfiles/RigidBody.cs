﻿using UnityEngine;
using System.Collections;

public class RigidBody : MonoBehaviour {

    public enum BodyTypes
    {
        Dynamic, 
        Static,
        Kinematic
    };

    public float Mass;
    public Vector2 Velocity;
    public Vector2 Acceleration;
    public Vector2 Position;
    
	
}
